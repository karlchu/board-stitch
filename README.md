# README for board-stitch

A simple python script to stitch together a number of images horizontally.

## Prerequisites and Installation

_Note: This script (and instructions here) are minimally tested on OSX only._

This script requires the `Pillow` python module to run.
To install it, open up the terminal in OSX:

- Press the `Command` key and the `space` bar together to bring up the Spotlight search box
- Type in `terminal`, and the terminal app will open up

In the terminal, run the following commands in the terminal:

```bash
sudo easy_install pip
sudo pip install Pillow
```

Note: Installing `pip` and `Pillow` requires administrative rights on the machine. The `sudo` command will ask you for your password.

## Usage

Download the lastest `board-stitch.py` script [here](https://bitbucket.org/karlchu/board-stitch/raw/HEAD/board-stitch.py).

The `python` interpreter is preinstalled on all OSX machines.
To run the script, use the command below in the terminal.

```
  python /Users/kchu/Downloads/board-stitch.py image1.jpg image2.jpg
#        ^-----------------------------------^ ^-------------------^
#         Path to the board-stitch.py script       List of images
```

Note: Only the line that starts with `python` is the command.
The other lines are just annotatations explaining what the command is.

Once the command finishes, combined image is saved as `stitched.jpg` in the current folder/directory of the terminal.
You may run the following command to open the stitched image in the Preview app.

```
open stitched.jpg
```
