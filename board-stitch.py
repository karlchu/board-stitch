#!/usr/bin/env python

import argparse
import sys
import os.path
from PIL import Image


def scale_image(img, to_width=0, to_height=0):
	if to_width > 0 and to_height > 0:
		new_width = to_width
		new_height = to_height
	elif to_height > 0:
		new_height = to_height
		new_width = int(float(img.size[0]) * float(new_height / float(img.size[1])))
	elif to_width > 0:
		new_width = to_width
		new_height = int(float(img.size[1]) * float(new_width / float(img.size[0])))
	else:
		return img

	return img.resize((new_width, new_height), Image.ANTIALIAS)


parser = argparse.ArgumentParser()
parser.add_argument('image1', help='First image')
parser.add_argument('image', nargs='+', help='Subsequent images to stitch')
parser.add_argument('-t', '--vertical', action='store_true', help='Stitch the images vertically instead')
args = parser.parse_args()

file_names = [args.image1] + args.image
for f in file_names:
	if not (os.path.isfile(f) and os.access(f, os.R_OK)):
		print "File '%(fn)s' does not exist." % { "fn": f }
		sys.exit(2)

images = map(Image.open, file_names)

if args.vertical:
	new_width = max(images, key=lambda img:img.size[0]).size[0]
	scaled_images = map(lambda img: scale_image(img, to_width=new_width), images)
	new_height = reduce(lambda sum, img: sum + img.size[1], scaled_images, 0)

	combined_image = Image.new('RGB', (new_width, new_height))

	offset = 0
	for im in scaled_images:
		combined_image.paste(im, (0, offset))
		offset += im.size[1]
else:
	new_height = max(images, key=lambda img:img.size[1]).size[1]
	scaled_images = map(lambda img: scale_image(img, to_height=new_height), images)
	new_width = reduce(lambda sum, img: sum + img.size[0], scaled_images, 0)

	combined_image = Image.new('RGB', (new_width, new_height))

	offset = 0
	for im in scaled_images:
		combined_image.paste(im, (offset, 0))
		offset += im.size[0]

combined_image.save('stitched.jpg')
