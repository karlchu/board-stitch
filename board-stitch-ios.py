import console
import dialogs
import photos
import sys
import webbrowser
from PIL import Image


def scale_image(img, to_width=0, to_height=0):
	if to_width > 0 and to_height > 0:
		new_width = to_width
		new_height = to_height
	elif to_height > 0:
		new_height = to_height
		new_width = int(float(img.size[0]) * float(new_height / float(img.size[1])))
	elif to_width > 0:
		new_width = to_width
		new_height = int(float(img.size[1]) * float(new_width / float(img.size[0])))
	else:
		return img

	return img.resize((new_width, new_height), Image.ANTIALIAS)

orientation = dialogs.list_dialog(
    title='Stitch Orientation', 
    items=['Horizontal', 'Vertical'])

select_another_photo = True
assets = []
while select_another_photo:
	for asset in (photos.pick_asset(title='Pick photos', multi=True)):
		assets.append(asset)
	selection = console.alert("Select Photo", "Select another photo?", "Yes", "No", hide_cancel_button=True)
	if selection == 2:
		break

if len(assets) < 2:
	console.hud_alert("You must select two or more images.", 'error')
	sys.exit(1)

images = map(lambda asset: asset.get_image(), assets)

if orientation == 'Vertical':
	new_width = max(images, key=lambda img:img.size[0]).size[0]
	scaled_images = map(lambda img: scale_image(img, to_width=new_width), images)
	new_height = reduce(lambda sum, img: sum + img.size[1], scaled_images, 0)

	combined_image = Image.new('RGB', (new_width, new_height))

	offset = 0
	for im in scaled_images:
		combined_image.paste(im, (0, offset))
		offset += im.size[1]
else:
	new_height = max(images, key=lambda img:img.size[1]).size[1]
	scaled_images = map(lambda img: scale_image(img, to_height=new_height), images)
	new_width = reduce(lambda sum, img: sum + img.size[0], scaled_images, 0)

	combined_image = Image.new('RGB', (new_width, new_height))

	offset = 0
	for im in scaled_images:
		combined_image.paste(im, (offset, 0))
		offset += im.size[0]

combined_image.save('stitched.jpg')
new_asset = photos.create_image_asset('stitched.jpg')

webbrowser.open("photos-redirect:")

